SELECT
    job.dt,
    job.job_id,
    d.job_type,
    SUM(job.job_detail_views) AS detail_views,
    SUM(job.job_apply_clicks) AS apply_clicks
FROM
    job_performance job
    LEFT JOIN job_details d USING (job_id)
WHERE
    job.job_status = "active"
GROUP BY
    job.dt,
    job.job_id,
    d.job_type
ORDER BY
    job.dt,
    job.job_id